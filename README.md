# cle-idp2-declercq-delafosse

Identity Provider 6 pour le projet CLE

## Spécification 

La [description de la spécification](https://notes.hoohoot.org/DQfQYCQjQviSOBbIJbYldA#) (Une version simplifiée de SAML) implémenté. 


## Mode d'authentification

- [x] TOTP 
- [ ] Emprunte de navigateur 


## Run 

### Developpement 

```
mvn compile quarkus:dev -Ddebug=true
```

## Build 

### Docker multistage 

- Pour build une image native : 

```sh
docker build -f src/main/docker/Dockerfile.multistage -t cle/idp6:0.0.0.1 .
```

- Pour build une image JVM : 

```sh
docker build -f src/main/docker/Dockerfile.jvm -t cle/idp6:0.0.0.1 .
```


## Endpoints

### Swagger : 

La documentation de l'API rest est disponible sur la route suivante : `/swagger-ui`. 

Il est également possible de récupérer un certain nombre de metrics sur les routes suivantes :

- **Metrics** : `/metrics`


    ```
    curl -X GET -H "Accept: application/json" localhost:8080/metrics
    ```
Pour plus d'informations voir : [Quarkus - MicroProfile Metrics](https://quarkus.io/guides/metrics-guide)

- **Health** : 
    - Readyness : `/health/ready`
    ```
    curl -X GET -H "Accept: application/json" localhost:8080/health/ready
    ```
    - Liveness : `/health/live`
    ```
    curl -X GET -H "Accept: application/json" localhost:8080/health/live
    ```
Pour plus d'informations :[Quarkus - MicroProfile Health](https://quarkus.io/guides/health-guide)
