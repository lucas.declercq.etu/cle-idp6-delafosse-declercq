import "./css/style.css";
import {Credentials} from "./model/credentials.model";
import {Login} from "./model/login.model";
import {ISAMLRequest} from "./model/saml-request.model";
import {ISAMLResponse} from "./model/saml-response.model";

document.getElementById("generate-qr-code-btn")
    .addEventListener("click", register);

/**
 * Register an user using an email
 */
function register(): void {
    const email = (document.getElementById("registerEmail") as HTMLInputElement).value;

    fetch("/sso/register", {
        body: JSON.stringify(email),
        headers: {"Accept": "application/json", "Content-Type": "application/json"},
        method: "POST",
    }).then((response: Response) => {
        return response.json();
    }).then((registerationJson) => {
        const img = new Image();
        const div = document.getElementById("email-gp");

        img.onload = () => div.appendChild(img);
        img.src = registerationJson.qrCodeUrl;

        // Hide button to prevent regeneration
        $("#generate-qr-code-btn").hide();
    });
}

/**
 * Login an user using email and time-based password
 */
function login(): void {
    event.preventDefault();

    const email = (document.getElementById("email") as HTMLInputElement).value;
    const totpCode = (document.getElementById("password") as HTMLInputElement).value;
    const samlRequest = parseSAMLRequestFromURL();

    const credentials = new Credentials(email, Number(totpCode));
    const loginBody = new Login(credentials, samlRequest);

    fetch("/sso/login", {
        body: JSON.stringify(loginBody),
        headers: {"Accept": "application/json", "Content-Type": "application/json"},
        method: "POST",
    }).then((response) => response.json() as Promise<ISAMLResponse>)
        .then((samlResponse) => buildForm(samlResponse).submit())
        .catch((err) => console.error(err));
}

/**
 * Build a form element and add it to DOM, based on an SAMLResponse
 * @param samlResponse
 */
function buildForm(samlResponse: ISAMLResponse): HTMLFormElement {
    const form = document.createElement("form");
    document.body.appendChild(form);
    form.action = samlResponse.assertionConsumerServiceURL;
    form.method = "POST";

    const success = createFormElement("success", samlResponse.success);
    const acs = createFormElement("assertionConsumerServiceURL", samlResponse.assertionConsumerServiceURL);
    const issuer = createFormElement("issuer", samlResponse.issuer);
    const userId = createFormElement("user_id", samlResponse.user_id);

    form.appendChild(success);
    form.appendChild(acs);
    form.appendChild(issuer);
    form.appendChild(userId);

    return form;
}

/**
 *
 * @param name
 * @param value
 */
function createFormElement(name: string, value: any): HTMLElement {
    const formElement = document.createElement("input");
    formElement.setAttribute("type", "input");
    formElement.setAttribute("value", value);
    formElement.setAttribute("name", name);
    return formElement;
}

/**
 *
 * @param errorMessage
 */
function showLoginError(errorMessage: string): void {

}

/**
 * Decode a base64 encoded SAMLRequest from the browser location href
 */
function parseSAMLRequestFromURL(): ISAMLRequest {
    const encodedSAMLRequest = new URL(document.location.href)
        .searchParams
        .get("request");

    const decodedSAMLRequest = atob(encodedSAMLRequest);
    return JSON.parse(decodedSAMLRequest);
}

$(() => {
    $("input[type='password'][data-eye]").each((i) => {
        const $this = $(this);
        const id = "eye-password-" + i;
        const el = $("#" + id);

        $this.wrap($("<div/>", {
            id: {id},
            style: "position:relative",
        }));

        $this.css({
            paddingRight: 60,
        });
        $this.after($("<div/>", {
            class: "btn btn-primary btn-sm",
            html: "Show",
            id: "passeye-toggle-" + i,
        }).css({
            cursor: "pointer",
            fontSize: 12,
            padding: "2px 7px",
            position: "absolute",
            right: 10,
            top: ($this.outerHeight() / 2) - 12,
        }));

        $this.after($("<input/>", {
            id: "passeye-" + i,
            type: "hidden",
        }));

        const invalidFeedback = $this
            .parent()
            .parent()
            .find(".invalid-feedback");

        if (invalidFeedback.length) {
            // @ts-ignore
            $this.after(invalidFeedback.clone());
        }

        $this.on("keyup paste", () => {
            $("#passeye-" + i).val($(this).val());
        });

        $("#passeye-toggle-" + i).on("click", () => {
            if ($this.hasClass("show")) {
                $this.attr("type", "password");
                $this.removeClass("show");
                $(this).removeClass("btn-outline-primary");
            } else {
                $this.attr("type", "text");
                $this.val($("#passeye-" + i).val());
                $this.addClass("show");
                $(this).addClass("btn-outline-primary");
            }
        });
    });

    $(".my-login-validation").submit(function () {
        const form = $(this);

        event.preventDefault();
        event.stopPropagation();

        if ((form[0] as HTMLFormElement).checkValidity() === true) {
            form.addClass("was-validated");
            login();
        }

    });
});
