import {Credentials} from "./credentials.model";
import {ISAMLRequest} from "./saml-request.model";

export class Login {
    public credentials: Credentials;
    public samlRequest: ISAMLRequest;

    constructor(credentials: Credentials, samlRequest: ISAMLRequest) {
        this.credentials = credentials;
        this.samlRequest = samlRequest;
    }
}
