export interface ISAMLResponse {
    assertionConsumerServiceURL: string;
    issuer: string;
    user_id: string;
    success: boolean;
}
