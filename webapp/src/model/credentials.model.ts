export class Credentials {
    public email: string;
    public oneTimePassword: number;

    constructor(email: string, oneTimePassword: number) {
        this.email = email;
        this.oneTimePassword = oneTimePassword;
    }
}
