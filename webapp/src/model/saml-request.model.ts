export interface ISAMLRequest {
    assertionConsumerServiceURL: string;
    issuer: string;
}
