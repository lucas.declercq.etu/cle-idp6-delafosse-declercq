package org.lille1.cle.entrypoints.rest.authentication.dto;

import org.lille1.cle.domain.authentication.LoginCredentials;
import org.lille1.cle.domain.saml.SAMLRequest;

public class LoginDTO {
    private LoginCredentials credentials;
    private SAMLRequest samlRequest;

    public LoginDTO() {
    }

    public LoginDTO(LoginCredentials credentials, SAMLRequest samlRequest) {
        this.credentials = credentials;
        this.samlRequest = samlRequest;
    }

    public LoginCredentials getCredentials() {
        return credentials;
    }

    public void setCredentials(LoginCredentials credentials) {
        this.credentials = credentials;
    }

    public SAMLRequest getSamlRequest() {
        return samlRequest;
    }

    public void setSamlRequest(SAMLRequest samlRequest) {
        this.samlRequest = samlRequest;
    }
}
