package org.lille1.cle.entrypoints.rest.authentication;

import org.eclipse.microprofile.metrics.MetricUnits;
import org.eclipse.microprofile.metrics.annotation.Counted;
import org.eclipse.microprofile.metrics.annotation.Timed;
import org.eclipse.microprofile.openapi.annotations.Operation;
import org.lille1.cle.domain.authentication.EmailAlreadyRegisteredException;
import org.lille1.cle.domain.authentication.LoginCredentials;
import org.lille1.cle.domain.authentication.TOTPKey;
import org.lille1.cle.domain.saml.SAMLResponse;
import org.lille1.cle.entrypoints.rest.Endpoint;
import org.lille1.cle.entrypoints.rest.authentication.dto.LoginDTO;
import org.lille1.cle.entrypoints.rest.authentication.dto.RegistrationDTO;
import org.lille1.cle.usecase.authentication.Login;
import org.lille1.cle.usecase.authentication.Register;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.security.GeneralSecurityException;

@Path("/sso")
public class AuthenticationEndpoint {

    @Inject
    Register register;

    @Inject
    Login login;

    @POST
    @Path(Endpoint.REGISTER)
    @Counted(name = "performedRegistrations", description = "How many registration were performed.")
    @Timed(name = "registrationTimer", description = "A measure on how long a registration took.", unit = MetricUnits.MILLISECONDS)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Register an user")
    public Response register(@Valid RegistrationDTO registration) {
        try {
            final TOTPKey totpKey = register.register(registration.getEmail());
            return Response.ok(totpKey).build();
        } catch (EmailAlreadyRegisteredException e) {
            return Response
                    .status(Response.Status.CONFLICT)
                    .entity(e.getMessage())
                    .build();
        }
    }

    @POST
    @Path(Endpoint.LOGIN)
    @Counted(name = "performedLogins", description = "How many login were performed.")
    @Timed(name = "loginTimer", description = "A measure on how long a login took.", unit = MetricUnits.MILLISECONDS)
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Operation(summary = "Login a user")
    public Response login(LoginDTO loginDTO) throws GeneralSecurityException {
        final LoginCredentials credentials = loginDTO.getCredentials();
        final boolean loginSuccess = this.login.login(credentials.getEmail(), credentials.getOneTimePassword());

        if (loginSuccess) {
            final SAMLResponse samlResponse = SAMLResponse.fromSAMLRequest(loginDTO.getSamlRequest(), credentials.getEmail(), true);
            return Response.ok()
                    .entity(samlResponse)
                    .build();
        }

        return Response.status(Response.Status.UNAUTHORIZED).build();
    }
}