package org.lille1.cle.entrypoints.rest.observability;

import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.Readiness;

@Readiness
public class ReadinessEndpoint implements HealthCheck {
    @Override
    public HealthCheckResponse call() {
        //TODO :  implement user storage check
        return HealthCheckResponse.named("User storage is responding to requests").up().build();
    }
}
