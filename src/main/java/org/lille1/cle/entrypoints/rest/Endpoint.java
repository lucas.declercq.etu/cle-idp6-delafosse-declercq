package org.lille1.cle.entrypoints.rest;

public class Endpoint {

    private Endpoint() {}

    public static final String LOGIN = "/login";
    public static final String REGISTER = "/register";
}
