package org.lille1.cle.entrypoints.rest.authentication.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

public class RegistrationDTO {

    @Email
    @NotEmpty
    private String email;

    public RegistrationDTO() {
    }

    public RegistrationDTO(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Registration{" +
                "email='" + email + '\'' +
                '}';
    }
}
