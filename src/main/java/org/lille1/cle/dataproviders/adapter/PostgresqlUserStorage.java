package org.lille1.cle.dataproviders.adapter;

import org.lille1.cle.dataproviders.adapter.entity.UserEntity;
import org.lille1.cle.dataproviders.port.UserStorage;
import org.lille1.cle.domain.authentication.EmailAlreadyRegisteredException;

import javax.ws.rs.NotFoundException;

public class PostgresqlUserStorage implements UserStorage {
    @Override
    public void add(String email, String secret) throws EmailAlreadyRegisteredException {
        new UserEntity(email, secret).persist();
    }

    @Override
    public String getSecret(String email) {
        UserEntity user = UserEntity
                .find("email = :email", email)
                .firstResult();

        if (user == null) {
            throw new NotFoundException();
        }

        return  user.getEmail();
    }
}
