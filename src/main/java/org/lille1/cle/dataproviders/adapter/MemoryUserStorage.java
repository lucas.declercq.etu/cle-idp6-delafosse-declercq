package org.lille1.cle.dataproviders.adapter;

import org.lille1.cle.dataproviders.port.UserStorage;
import org.lille1.cle.domain.authentication.EmailAlreadyRegisteredException;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import java.util.HashMap;
import java.util.Map;

@Default
@ApplicationScoped
public class MemoryUserStorage implements UserStorage {
    private Map<String, String> registeredUsers = new HashMap<>();

    @Override
    public void add(String email, String secret) throws EmailAlreadyRegisteredException {
        if (registeredUsers.containsKey(email)) {
            throw new EmailAlreadyRegisteredException(email);
        }

        registeredUsers.put(email, secret);
    }

    @Override
    public String getSecret(String email) {
        return registeredUsers.get(email);
    }
}
