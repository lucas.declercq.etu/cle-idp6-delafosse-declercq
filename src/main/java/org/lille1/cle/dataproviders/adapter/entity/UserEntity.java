package org.lille1.cle.dataproviders.adapter.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

public class UserEntity extends PanacheEntity {
    private String email;
    private String totpSecret;

    public UserEntity(String email, String totpSecret) {
        this.email = email;
        this.totpSecret = totpSecret;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTotpSecret() {
        return totpSecret;
    }

    public void setTotpSecret(String totpSecret) {
        this.totpSecret = totpSecret;
    }
}
