package org.lille1.cle.dataproviders.port;

import org.lille1.cle.domain.authentication.EmailAlreadyRegisteredException;

public interface UserStorage {
    void add(String email, String secret) throws EmailAlreadyRegisteredException;

    String getSecret(String email);
}
