package org.lille1.cle.usecase.authentication;

import com.j256.twofactorauth.TimeBasedOneTimePasswordUtil;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.lille1.cle.dataproviders.port.UserStorage;
import org.lille1.cle.domain.authentication.TOTPKey;
import org.lille1.cle.domain.authentication.EmailAlreadyRegisteredException;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class Register {

    @ConfigProperty(name = "quarkus.application.name")
    String idpId;

    @Inject
    UserStorage userStorage;

    public TOTPKey register(String email) throws EmailAlreadyRegisteredException {
        final String base32Secret = TimeBasedOneTimePasswordUtil.generateBase32Secret();
        userStorage.add(email, base32Secret);

        final String qrCodeUrl = TimeBasedOneTimePasswordUtil.qrImageUrl(String.format("%s - %s", idpId, email), base32Secret);

        return new TOTPKey(base32Secret, qrCodeUrl);
    }
}
