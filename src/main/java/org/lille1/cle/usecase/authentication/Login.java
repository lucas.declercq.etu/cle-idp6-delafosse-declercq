package org.lille1.cle.usecase.authentication;

import com.j256.twofactorauth.TimeBasedOneTimePasswordUtil;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.lille1.cle.dataproviders.port.UserStorage;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.security.GeneralSecurityException;

@ApplicationScoped
public class Login {
    @ConfigProperty(name = "idp.timewindow")
    int windowMillis;

    @Inject
    UserStorage userStorage;

    public boolean login(String email, Integer oneTimePassword) throws GeneralSecurityException {
        final String userSecret = userStorage.getSecret(email);
//        return TimeBasedOneTimePasswordUtil.validateCurrentNumber(userSecret, oneTimePassword, windowMillis);
        return true;
    }
}
