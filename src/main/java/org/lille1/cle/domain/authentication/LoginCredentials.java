package org.lille1.cle.domain.authentication;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

public class LoginCredentials {
    @Email
    @NotEmpty
    private String email;

    @Positive
    private Integer oneTimePassword;

    public LoginCredentials() {
    }

    public LoginCredentials(String email, Integer oneTimePassword) {
        this.email = email;
        this.oneTimePassword = oneTimePassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getOneTimePassword() {
        return oneTimePassword;
    }

    public void setOneTimePassword(Integer oneTimePassword) {
        this.oneTimePassword = oneTimePassword;
    }
}
