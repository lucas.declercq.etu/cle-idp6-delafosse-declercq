package org.lille1.cle.domain.authentication;

import java.util.Objects;

public class TOTPKey {
    private String key;
    private String qrCodeUrl;

    public TOTPKey() { }

    public TOTPKey(String key, String qrCodeUrl) {
        this.key = key;
        this.qrCodeUrl = qrCodeUrl;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public void setQrCodeUrl(String qrCodeUrl) {
        this.qrCodeUrl = qrCodeUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TOTPKey totpKey = (TOTPKey) o;
        return Objects.equals(key, totpKey.key) &&
                Objects.equals(qrCodeUrl, totpKey.qrCodeUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, qrCodeUrl);
    }
}
