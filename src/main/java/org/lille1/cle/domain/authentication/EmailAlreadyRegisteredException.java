package org.lille1.cle.domain.authentication;

public class EmailAlreadyRegisteredException extends Exception {
    public EmailAlreadyRegisteredException(String email) {
        super(String.format("This email is already registered : %s", email));
    }
}
