package org.lille1.cle.domain.saml;

public class SAMLRequest {
    private String assertionConsumerServiceURL;
    private String issuer;

    public SAMLRequest() {
    }

    public SAMLRequest(String assertionConsumerServiceURL, String issuer) {
        this.assertionConsumerServiceURL = assertionConsumerServiceURL;
        this.issuer = issuer;
    }

    public String getAssertionConsumerServiceURL() {
        return assertionConsumerServiceURL;
    }

    public void setAssertionConsumerServiceURL(String assertionConsumerServiceURL) {
        this.assertionConsumerServiceURL = assertionConsumerServiceURL;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }
}
