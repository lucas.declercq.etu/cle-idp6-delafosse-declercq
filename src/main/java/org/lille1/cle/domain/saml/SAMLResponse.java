package org.lille1.cle.domain.saml;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SAMLResponse {
    private String assertionConsumerServiceURL;
    private String issuer;
    @JsonProperty("user_id")
    private String userId;
    private boolean success;

    public SAMLResponse(String assertionConsumerServiceURL, String issuer, String userId, boolean success) {
        this.assertionConsumerServiceURL = assertionConsumerServiceURL;
        this.issuer = issuer;
        this.userId = userId;
        this.success = success;
    }

    private SAMLResponse(SAMLRequest samlRequest, String userId, boolean status) {
        this.assertionConsumerServiceURL = samlRequest.getAssertionConsumerServiceURL();
        this.issuer = samlRequest.getIssuer();
        this.userId = userId;
        this.success = status;
    }

    public static SAMLResponse fromSAMLRequest(SAMLRequest request, String userId, boolean status) {
        return new SAMLResponse(request, userId, status);
    }

    public String getAssertionConsumerServiceURL() {
        return assertionConsumerServiceURL;
    }

    public void setAssertionConsumerServiceURL(String assertionConsumerServiceURL) {
        this.assertionConsumerServiceURL = assertionConsumerServiceURL;
    }

    public String getIssuer() {
        return issuer;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
