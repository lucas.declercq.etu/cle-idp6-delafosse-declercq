package org.lille1.cle.domain.saml;

public class SAMLRequestFixture {
    public static SAMLRequest validSAMLRESamlRequest() {
        return new SAMLRequest("https://iagl.sp.fr/saml", "https://iagl.sp.fr/ressource");
    }
}