package org.lille1.cle.domain.saml;

public class SAMLResponseFixture {
    public static SAMLResponse onSuccessSamlResponse() {
        return new SAMLResponse(
                "https://iagl.sp.fr/saml",
                "https://iagl.sp.fr/ressource",
                "paul.delafosse@kiikoo.org",
                true
        );
    }

    public static SAMLResponse onFailureSamlResponse() {
        return new SAMLResponse(
                "https://iagl.sp.fr/saml",
                "https://iagl.sp.fr/ressource",
                "lucas.declercq@cookies.gro",
                false
        );
    }
}