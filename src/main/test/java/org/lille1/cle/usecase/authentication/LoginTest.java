package org.lille1.cle.usecase.authentication;

import io.quarkus.test.Mock;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.lille1.cle.dataproviders.port.UserStorage;
import org.lille1.cle.domain.authentication.EmailAlreadyRegisteredException;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class LoginTest {

    @Inject
    Login login;

    @Test
    void should_login_a_known_user_with_a_matching_otp_password() throws GeneralSecurityException {
        // arrange
        // voir UserStorageMock
        String validEmail = "paul.delafosse@kikoo.org";
        String validPassword = "12345";

        // act
        boolean login = this.login.login(validEmail, Integer.parseInt(validPassword));

        // assert
        assertTrue(login);
    }

    @Test
    void should_not_login_a_known_user_with_a_wrong_otp_password() throws GeneralSecurityException {
        // arrange
        // see: UserStorageMock.class
        String validEmail = "paul.delafosse@kikoo.org";
        String invalidPassword = "999999";

        // act
        boolean login = this.login.login(validEmail, Integer.parseInt(invalidPassword));

        // assert
        assertFalse(login);
    }

    @Test
    void should_not_login_unknown_user() throws GeneralSecurityException {
        // arrange
        String validEmail = "steve.labnik@rust.lang";
        String invalidPassword = "0112358";

        // act
        boolean login = this.login.login(validEmail, Integer.parseInt(invalidPassword));

        // assert
        assertFalse(login);
    }

    @Mock
    @ApplicationScoped
    static class UserStorageMock implements UserStorage {
        private Map<String, String> registeredUsers = new HashMap<>();

        public UserStorageMock() {
            this.registeredUsers.put("paul.delafosse@kikoo.org", "12345");
            this.registeredUsers.put("lucas.declerq@cookie.gros", "54321");
        }

        @Override
        public void add(String email, String secret) throws EmailAlreadyRegisteredException {}

        @Override
        public String getSecret(String email) {
            return this.registeredUsers.get(email);
        }
    }
}