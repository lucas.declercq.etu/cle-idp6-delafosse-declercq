package org.lille1.cle.usecase.authentication;

import io.quarkus.test.Mock;
import org.lille1.cle.domain.authentication.EmailAlreadyRegisteredException;
import org.lille1.cle.domain.authentication.TOTPKey;
import org.lille1.cle.domain.authentication.TOTPKeyFixture;
import org.lille1.cle.entrypoints.fixture.RegistrationFixture;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

@Mock
@Alternative
@Priority(1)
@ApplicationScoped
class RegisterMock extends Register {

    String registeredMail = RegistrationFixture.alreadyRegisteredUser().getEmail();

    @Override
    public TOTPKey register(String email) throws EmailAlreadyRegisteredException {
        if(email.equals(registeredMail)) {
            throw new EmailAlreadyRegisteredException(registeredMail);
        }
        return TOTPKeyFixture.getTOTPKey();
    }
}