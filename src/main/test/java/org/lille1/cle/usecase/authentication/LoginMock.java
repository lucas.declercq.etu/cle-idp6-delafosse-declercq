package org.lille1.cle.usecase.authentication;

import io.quarkus.test.Mock;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Alternative;

@Mock
@Alternative
@Priority(1)
@ApplicationScoped
class LoginMock extends Login {

    @Override
    public boolean login(String email, Integer oneTimePassword) {
        return true;
    }
}