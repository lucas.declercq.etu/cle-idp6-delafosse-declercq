package org.lille1.cle.entrypoints.rest.authentication;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import org.lille1.cle.domain.authentication.TOTPKey;
import org.lille1.cle.domain.authentication.TOTPKeyFixture;
import org.lille1.cle.entrypoints.fixture.RegistrationFixture;
import org.lille1.cle.entrypoints.rest.Endpoint;
import org.lille1.cle.entrypoints.rest.authentication.dto.RegistrationDTO;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class AuthenticationEndpointTest {

    ObjectMapper jsonMapper = new ObjectMapper();

    @Test
    void a_valid_registration_should_return_TOTPKEY() throws JsonProcessingException {
        // arrange
        RegistrationDTO validRegistration = RegistrationFixture.validRegistration();
        TOTPKey expectedKey = TOTPKeyFixture.getTOTPKey();

        // act + assert
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(validRegistration)
                .when()
                .post("/sso" + Endpoint.REGISTER)
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body(is(jsonMapper.writeValueAsString(expectedKey)));
    }

    @Test
    void a_invalid_registration_should_throw_error_409 () {
        // arrange
        RegistrationDTO invalidRegistration = RegistrationFixture.invalidRegistration();
        TOTPKey expectedKey = TOTPKeyFixture.getTOTPKey();

        // act + assert
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(invalidRegistration)
                .when()
                .post("/sso" + Endpoint.REGISTER)
                .then()
                .statusCode(Response.Status.BAD_REQUEST.getStatusCode());
    }

    @Test
    void register_a_known_user_should_throw_EmailAlreadyRegisteredException() {
        // arrange
        RegistrationDTO invalidRegistration = RegistrationFixture.alreadyRegisteredUser();
        TOTPKey expectedKey = TOTPKeyFixture.getTOTPKey();

        // act + assert
        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(invalidRegistration)
                .when()
                .post("/sso" + Endpoint.REGISTER)
                .then()
                .statusCode(Response.Status.CONFLICT.getStatusCode())
                .body(is("This email is already registered : paul.delafosse.etu@univ-lille.fr"));
    }

}