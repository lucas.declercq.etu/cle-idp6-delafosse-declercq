package org.lille1.cle.entrypoints.rest.authentication.dto;

import org.lille1.cle.domain.authentication.LoginCredentials;
import org.lille1.cle.domain.saml.SAMLRequestFixture;

public class LoginDTOFixture {
    public static LoginDTO validLoginDTO() {
        return new LoginDTO(new LoginCredentials("paul.delafosse@kikoo.rm", 12345),
                            SAMLRequestFixture.validSAMLRESamlRequest());
    }
}