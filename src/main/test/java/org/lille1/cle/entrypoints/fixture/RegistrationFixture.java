package org.lille1.cle.entrypoints.fixture;

import org.lille1.cle.entrypoints.rest.authentication.dto.RegistrationDTO;

public class RegistrationFixture {
    public static RegistrationDTO validRegistration() {
        return new RegistrationDTO("karl.marx@univ-lille.fr");
    }

    public static RegistrationDTO alreadyRegisteredUser() {
        return new RegistrationDTO("paul.delafosse.etu@univ-lille.fr");
    }

    public static RegistrationDTO invalidRegistration() {
        return new RegistrationDTO("arrrrg!");
    }
}
